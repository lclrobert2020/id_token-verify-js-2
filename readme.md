*install packages*

```
npm i axios
npm i jsonwebtoken
npm i node-rsa
```

*Import by*

```
const veriftyId_token = require('./veriftyId_token');
```

*How to use *

```
veriftyId_token(Id_token,[optional: nonce]).then{...}.catch{...}
```

*returns the verified token object when success and the error msg when failed

the verifiedTokenObject.sub is the unique identifier for the user*